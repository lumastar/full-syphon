# Full Syphon

Full Syphon is an OS X Cocoa application to display fullscreen Syphon video. It's based on the Syphon Simple Client demo application which can be found here: https://github.com/Syphon/Simple

# functionality and limitations

Full Syphon currently only works with one display per instance. The fullscreened content stays above all menus and windows displayed by OS X, even when the app is not actively selected.

The app does not handle changes in resolution to the fullscreened display, or displays dissapearing.
