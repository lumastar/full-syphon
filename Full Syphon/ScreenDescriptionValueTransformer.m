//
//  ScreenDescriptionValueTransformer.m
//  Simple Client
//
//  Created by William Squires on 2017-02-21.
//
//

#import "ScreenDescriptionValueTransformer.h"

@implementation ScreenDescriptionValueTransformer
+ (Class)transformedValueClass { return [NSString class]; }
+ (BOOL)allowsReverseTransformation { return NO; }

- (id)transformedValue:(id)value
{
    if ([value isKindOfClass:[NSArray class]])
    {
        NSMutableArray *transformed = [NSMutableArray arrayWithCapacity:[value count]];
        for (NSScreen *screen in value)
        {
            NSNumber *screenNumber = [[screen deviceDescription] objectForKey:@"NSScreenNumber"];
            NSString *screenTitle = [screenNumber stringValue];
            NSLog(@"%@", screenTitle);
            [transformed addObject:screenTitle];
        }
        return transformed;
    }
    return nil;
}
@end
