/*
    SimpleClientAppDelegate.m
	Syphon (SDK)
	
    Copyright 2010-2011 bangnoise (Tom Butterworth) & vade (Anton Marini).
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "FullSyphonAppDelegate.h"

@interface FullSyphonAppDelegate (Private)
- (void)resizeWindowForCurrentVideo;
@end

@implementation FullSyphonAppDelegate

+ (NSSet *)keyPathsForValuesAffectingStatus
{
    return [NSSet setWithObjects:@"frameWidth", @"frameHeight", @"FPS", @"selectedServerDescriptions", nil];
}

- (void)dealloc
{
    [selectedServerDescriptions release];
    [super dealloc];
}

@synthesize FPS;

@synthesize frameWidth;

@synthesize frameHeight;

@synthesize screens;

- (NSString *)status
{
    if (self.frameWidth && self.frameHeight)
    {
        return [NSString stringWithFormat:@"%lu x %lu : %lu FPS", (unsigned long)self.frameWidth, (unsigned long)self.frameHeight, (unsigned long)self.FPS];
    }
    else
    {
        return @"--";
    }
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
	return YES;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // We use an NSArrayController to populate the menu of available servers
    // Here we bind its content to SyphonServerDirectory's servers array
    [availableServersController bind:@"contentArray" toObject:[SyphonServerDirectory sharedDirectory] withKeyPath:@"servers" options:nil];
    
    // Slightly weird binding here, if anyone can neatly and non-weirdly improve on this then feel free...
    [self bind:@"selectedServerDescriptions" toObject:availableServersController withKeyPath:@"selectedObjects" options:nil];
    
    screens = [NSScreen screens];
    [availableScreensController bind:@"contentArray" toObject:self withKeyPath:@"screens" options:nil];
    
    //[self bind:@"selectedScreens" toObject:availableScreensController withKeyPath:@"selectedObjects" options:nil];
    
    [[glView window] setContentMinSize:(NSSize){400.0,300.0}];
	[[glView window] setDelegate:self];
}

- (NSArray *)selectedServerDescriptions
{
    return selectedServerDescriptions;
}

- (void)setSelectedServerDescriptions:(NSArray *)descriptions
{
    if (![descriptions isEqualToArray:selectedServerDescriptions])
    {
        [descriptions retain];
        [selectedServerDescriptions release];
        selectedServerDescriptions = descriptions;
        // Stop our current client
        [syClient stop];
        [syClient release];
        // Reset our terrible FPS display
        fpsStart = [NSDate timeIntervalSinceReferenceDate];
        fpsCount = 0;
        self.FPS = 0;
        syClient = [[SyphonClient alloc] initWithServerDescription:[descriptions lastObject]
                                                           context:[[glView openGLContext] CGLContextObj]
                                                           options:nil newFrameHandler:^(SyphonClient *client) {
            // This gets called whenever the client receives a new frame.
            
            // The new-frame handler could be called from any thread, but because we update our UI we have
            // to do this on the main thread.
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                // First we track our framerate...
                fpsCount++;
                float elapsed = [NSDate timeIntervalSinceReferenceDate] - fpsStart;
                if (elapsed > 1.0)
                {
                    self.FPS = ceilf(fpsCount / elapsed);
                    fpsStart = [NSDate timeIntervalSinceReferenceDate];
                    fpsCount = 0;
                }
                // ...then we check to see if our dimensions display or window shape needs to be updated
                SyphonImage *frame = [client newFrameImage];
                
                NSSize imageSize = frame.textureSize;
                
                if (self.frameWidth != imageSize.width)
                {
                    self.frameWidth = imageSize.width;
                }
                if (self.frameHeight != imageSize.height)
                {
                    self.frameHeight = imageSize.height;
                }

                // ...then update the view and mark it as needing display
                glView.image = frame;

                [glView setNeedsDisplay:YES];

                // newFrameImageForContext: returns a retained image, always release it
                [frame release];
            }];
        }];
        
        // If we have a client we do nothing - wait until it outputs a frame
        
        // Otherwise clear the view
        if (syClient == nil)
        {
            glView.image = nil;

            self.frameWidth = 0;
            self.frameHeight = 0;

            [glView setNeedsDisplay:YES];
        }
    }
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{		
	[syClient stop];
	[syClient release];
	syClient = nil;
}

- (IBAction)fullscreen:(id)sender
{
    NSScreen *selectedScreen = [[availableScreensController selectedObjects] lastObject];
    //NSLog(@"Fullscreen to display: %lu",(unsigned long)[[selectedScreen deviceDescription] objectForKey:@"NSScreenNumber"]);
    NSRect displayRect = [selectedScreen frame];
    NSWindow *fullScreenWindow = [[NSWindow alloc] initWithContentRect: displayRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:YES];
    
    [fullScreenWindow setLevel:NSMainMenuWindowLevel+1];
    
    [fullScreenWindow setOpaque:YES];
    [fullScreenWindow setHidesOnDeactivate:NO];
    
    [fullScreenWindow setContentView:glView];
    [fullScreenWindow makeKeyAndOrderFront:self];
}

@end
